#!/usr/bin/ruby
#
# Name: check_cfgmgt_status.rb
#
# Purpose: Loop over an array of hostnames and check their configuration
# management status in Satellite 6. Keep checking until all have the desired
# status.
#
# Default desired status: ['No changes', 'Active', 'Error']
#

begin
  @method = 'check_configuration_management_status'
  $evm.log('info', "#{@method} - EVM Automate Method Started")

  require 'rest-client'
  require 'json'

  $evm.root.attributes.sort.each { |k, v| $evm.log("info", 
    "#{@method} Root:<$evm.root> Attribute - #{k}: #{v}")}

  @foreman_host = $evm.root['dialog_sat6_host']
  @foreman_user = $evm.root['dialog_sat6_user']
  @foreman_password = $evm.root.decrypt('dialog_sat6_password')
  @provisioned_hosts = $evm.root['service_template_provision_task'].destination.vms

  # log each vm's uid_ems
  @provisioned_hosts.each do |vm|
    $evm.log("info", "VM id: <#{vm.uid_ems}>")
    $evm.log("info", "VM hostnames: <#{vm.hostnames}>")
    $evm.log("info", "VM fqdn: <#{vm.location}>")
  end

  # Create list of hostnames from list of objects; only use internal hostnames
  @provisioned_hosts_names = []
  @provisioned_hosts.each do |vm| 
    n = vm.hostnames.keep_if { |v| v =~ /^ip.*internal$/ }
    @provisioned_hosts_names.append(n[0])
  end

  # Logging all hostnames
  $evm.log("info", "--> All VM hostnames in the provisioned_hosts_names list:")
  @provisioned_hosts_names.each { |h|
    $evm.log("info", "VM hostname (from list): <#{h}>")
  }

  # Pass the list of VMs to $evm object
  $evm.log("info", "--> Passing @provisioned_hosts_names to $evm:")
  $evm.log("info", @provisioned_hosts_names.join(','))
  $evm.root['service_template_provision_task'].miq_request.set_option(:tower_limit_list, @provisioned_hosts_names.join(','))

	def get_json(location)
	  response = RestClient::Request.new(
		  :method => :get,
		  :url => location,
		  :verify_ssl => false,
		  :user => @foreman_user,
		  :password => @foreman_password,
		  :headers => { :accept => :json,
		  :content_type => :json }
	  ).execute
	  results = JSON.parse(response.to_str)
  end
  
  $evm.log('info', "#{@method} - Checking for all hosts in Satellite")
  max = 20
  now = 0
  allin = false
  while allin == false
    r = get_json('https://' + @foreman_host + '/api/hosts')
    all_hosts = []
    for host in r['results'] do
      all_hosts << host['name']
    end
    
    $evm.log('info', "#{@method} - Hosts in Satellite: #{all_hosts}")
    
    missed_one = false
    @provisioned_hosts_names.each do |host|
      host = host.tr('"','')
      missed_one = true unless all_hosts.any? { |h| h.include?(host) }
      if missed_one == true
        $evm.log('info', "#{@method} - Host #{host} was not found yet")
        break
      else
        $evm.log('info', "#{@method} - Found host #{host}")
      end
    end
    
    if missed_one == false
      allin = true
    end
    
    now = now + 1
    if now > max
      $evm.log('info', "#{@method} - This is taking too fucking long!")
      exit MIQ_ABORT
    end
    $evm.log('info', "#{@method} - Taking a nap")
    sleep 30 if missed_one == true
    
  end
  $evm.log('info', "#{@method} - All hosts found")
  $evm.log('info', "#{@method} - Creating real host list")
  $evm.log('info', "#{@method} - All host list: #{all_hosts}")
  $evm.log('info', "#{@method} - Patterns: #{@provisioned_hosts_names}")
  
  real_hosts = []
  for host in all_hosts do
    for pattern in @provisioned_hosts_names do
      if host.include?(pattern.tr('"',''))
        real_hosts << host
      end
    end
  end
  $evm.log('info', "#{@method} - Created real host list")
  $evm.log('info', "#{@method} - Real host list: #{real_hosts}")
  
  for host in real_hosts do
    $evm.log('info', "Checking configuration management status of #{host}")
    while true
      host = host.tr('"','')
      r = get_json('https://' + @foreman_host + '/api/hosts/' + host + '/status')
      break if ['Active', 'No changes', 'Error'].include? r['status']
      $evm.log('info', "Status: #{host} has not been fully configured yet, waiting 10 seconds")
      sleep(10)
    end
    $evm.log('info', "Status: #{host} has been fully configured, continuing")
  end

  $evm.log('info', "Status: all hosts have been fully configured, method ending")

	#
	# Exit method
	#
	$evm.log("info", "#{@method} - EVM Automate Method Ended")
	exit MIQ_OK

	#
	# Set Ruby rescue behavior
	#
rescue => err
	$evm.log("error", "#{@method} - [#{err}]\n#{err.backtrace.join("\n")}")
	exit MIQ_ABORT
end

exit()
