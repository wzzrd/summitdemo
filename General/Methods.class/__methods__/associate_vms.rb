#!/usr/bin/ruby
#
# Name: check_cfgmgt_status.rb
#
# Purpose: Loop over an array of hostnames and check their configuration
# management status in Satellite 6. Keep checking until all have the desired
# status.
#
# Default desired status: ['No changes', 'Active', 'Error']
#

begin
  @method = 'check_configuration_management_status'
  $evm.log('info', "#{@method} - EVM Automate Method Started")

  require 'rest-client'
  require 'json'

  $evm.root.attributes.sort.each { |k, v| $evm.log("info", 
    "#{@method} Root:<$evm.root> Attribute - #{k}: #{v}")}

  @foreman_host = $evm.root['dialog_sat6_host']
  @foreman_user = $evm.root['dialog_sat6_user']
  @foreman_password = $evm.root.decrypt('dialog_sat6_password')

  def put_json(location, json_data)
  	response = RestClient::Request.new(
  		:method => :put,
  		:url => location,
  		:verify_ssl => false,
  		:user => @foreman_user,
  		:password => @foreman_password,
  		:headers => { :accept => :json,
  		:content_type => :json},
  		:payload => json_data
  	).execute
  	results = JSON.parse(response.to_str)
  end
  
  put_json('https://' + @foreman_host + '/api/compute_resources/1/associate', '')
  
  $evm.log('info', "Status: all hosts have been associated with the compute resource")

	#
	# Exit method
	#
	$evm.log("info", "#{@method} - EVM Automate Method Ended")
	exit MIQ_OK

	#
	# Set Ruby rescue behavior
	#
rescue => err
	$evm.log("error", "#{@method} - [#{err}]\n#{err.backtrace.join("\n")}")
	exit MIQ_ABORT
end

exit()
