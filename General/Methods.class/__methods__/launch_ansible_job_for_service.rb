#!/usr/bin/ruby
#
# Name: trigger_job.rb
#
# Purpose: Trigger an Ansible job to be run
#

begin
  @method = 'launch_ansible_job_for_service'
  $evm.log('info', "#{@method} - EVM Automate Method Started")

  require 'rest-client'
  require 'json'

  $evm.root.attributes.sort.each { |k, v| $evm.log("info", 
   "#{@method} Root:<$evm.root> Attribute - #{k}: #{v}")}

  @tower_host = $evm.object['tower_host']
  @tower_user = $evm.object['tower_user']
  @tower_password = $evm.object.decrypt('tower_password')
  @job_template_id = $evm.object['tower_job_template_id']

  def auth(location)
    response = RestClient::Request.new(
      :method => :post,
      :url => location,
      :verify_ssl => false,
      :headers => { :accept => :json,
      :content_type => :json },
      :payload => JSON.generate({:username => @tower_user, 
                                 :password => @tower_password})
    ).execute
    results = JSON.parse(response.to_str)
    return results['token']
  end

  def get_json(location, json)
    response = RestClient::Request.new(
      :method => :get,
      :url => location,
      :verify_ssl => false,
      :headers => { :accept => :json,
      :authentication => "Token #{@token}",
      :content_type => :json },
      :payload => json
    ).execute
    results = JSON.parse(response.to_str)
  end

  def post_json(location, json)
    response = RestClient::Request.new(
      :method => :post,
      :url => location,
      :verify_ssl => false,
      :headers => { :accept => :json,
      :authorization => "Token #{@token}",
      :content_type => :json },
      :payload => json
    ).execute
    results = JSON.parse(response.to_str)
  end

  @token = auth('https://' + @tower_host + '/api/v1/authtoken/')

  jt_url = 'https://' + @tower_host + '/api/v1/job_templates/' + @job_template_id.to_s + '/launch/'

  puts "Posting with Token #{@token}"

  started_job = post_json(jt_url, '')

  @job = started_job['id']

  $evm.log('info', "Ansible job #{@job} has started")
  $evm.set_state_var(:ansible_job_id, @job)

	#
	# Exit method
	#
	$evm.log("info", "#{@method} - EVM Automate Method Ended")
	exit MIQ_OK

	#
	# Set Ruby rescue behavior
	#
rescue => err
	$evm.log("error", "#{@method} - [#{err}]\n#{err.backtrace.join("\n")}")
	exit MIQ_ABORT
end
