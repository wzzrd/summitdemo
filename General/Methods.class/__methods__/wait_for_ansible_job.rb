#!/usr/bin/ruby
#
# Name: wait_for_job.rb
#
# Purpose: Wait for an Ansible job to be completed
#

begin
  @method = 'wait_for_job'
  $evm.log('info', "#{@method} - EVM Automate Method Started")
  
  require 'rest-client'
  require 'json'

  $evm.root.attributes.sort.each { |k, v| $evm.log("info", 
   "#{@method} Root:<$evm.root> Attribute - #{k}: #{v}")}

  @tower_host = $evm.object['tower_host']
  @tower_user = $evm.object['tower_user']
  @tower_password = $evm.object.decrypt('tower_password')
  @job = $evm.get_state_var(:ansible_job_id)
  
  def auth(location)
    response = RestClient::Request.new(
      :method => :post,
      :url => location,
      :verify_ssl => false,
      :headers => { :accept => :json,
      :content_type => :json },
      :payload => JSON.generate({:username => @tower_user, 
                                 :password => @tower_password})
    ).execute
    results = JSON.parse(response.to_str)
    return results['token']
  end

  def get_json(location, json)
    response = RestClient::Request.new(
      :method => :get,
      :url => location,
      :verify_ssl => false,
      :headers => { :accept => :json,
      :authorization => "Token #{@token}",
      :content_type => :json },
      :payload => json
    ).execute
    results = JSON.parse(response.to_str)
  end

  def post_json(location, json)
    response = RestClient::Request.new(
      :method => :post,
      :url => location,
      :verify_ssl => false,
      :headers => { :accept => :json,
      :authorization => "Token #{@token}",
      :content_type => :json },
      :payload => json
    ).execute
    results = JSON.parse(response.to_str)
  end

  @token = auth('https://' + @tower_host + '/api/v1/authtoken/')

  job_url = 'https://' + @tower_host + '/api/v1/jobs/' + @job.to_s + '/'

  $evm.log('info', "Checking status of job #{@job} with Token #{@token}")

  done = false
  while done == false
    $evm.log('info', "Getting info for job #{@job}")
    started_job = get_json(job_url, '')
    done = true unless started_job['finished'].nil?
    $evm.log('info', "Job #{@job} status is #{started_job['status']}")
    unless done == true
      $evm.log('info', "Job #{@job} is not done yet, taking a 10 second nap...")
      sleep 10
    end 
  end

  $evm.log('info', "Ansible job #{@job} has finished")

	#
	# Exit method
	#
	$evm.log("info", "#{@method} - EVM Automate Method Ended")
	exit MIQ_OK

	#
	# Set Ruby rescue behavior
	#
rescue => err
	$evm.log("error", "#{@method} - [#{err}]\n#{err.backtrace.join("\n")}")
	exit MIQ_ABORT
end
